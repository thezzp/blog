const mongoose=require('mongoose')
const Schema=mongoose.Schema;
const crypto=require("crypto")

let userSchema=new mongoose.Schema({
    user:{
        type:String,
        required:true,
        unique:true,
        match:/^[\w\u4e00-\u9fa5]{2,10}$/
    },
    pwd:{
        type:String,
        required: true,
    },
    email:{
        type:String,
        required: true,
        match:/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/
    },
    phone:{
        type:String,
        match:/^1[3-9]\d{9}$/
    },
    age:{
        type:Number
    },
    sex:{
        type:String,
    },
    avatar:{type:String,default:'http://localhost:3002/public/image/1589190907744.jpg'},
    signature:{type:String,default:'这人很懒,没留下什么!'},

})
//密码加密的方法
//在被保存到数据库之前,加密pwd
//加密固定写法,update表示要加密的数据
userSchema.pre("save",function (next) {
    // console.log("save中间件执行了");

    this.pwd=crypto.createHash("sha256").update(this.pwd).digest("hex")
    console.log(this)
    next();
})



module.exports=mongoose.model("user",userSchema)
