const mongoose=require('mongoose')
mongoose.set('useCreateIndex', true)
mongoose.connect('mongodb://localhost/blog',{useNewUrlParser:true, useUnifiedTopology: true });

//链接数据库
let db=mongoose.connection;//数据库的链接对象

db.on('error',console.error.bind(console,'connection error'));
db.once('open',function () {
    console.log('db ok')
});
