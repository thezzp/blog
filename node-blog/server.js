const express =require('express')
const db=require('./db/connect')
const app=express()
const bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())
app.use(express.json())
app.use('/public',express.static(__dirname+'/static'))

app.set("secret",'aa1sd45w1')


//用express的中间件cors解决跨域问题
const cors=require('cors')
app.use(cors())



require('./router/login')(app)
require('./router/register')(app)
require("./router/resetPwd")(app)
require("./router/forgetPwd")(app)
require("./router/getMailCode")(app)
require('./router/avatar')(app)
require("./router/userCenter")(app)
require("./router/art/article")(app)
require("./router/art/comment")(app)
//点赞功能
require("./router/art/good")(app)


//上传图片
app.use('/',require("./router/upload"))

app.listen(3002,()=>{
    console.log('serve start')
})
