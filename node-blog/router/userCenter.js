module.exports = app => {
    const express = require('express')
    const router = express.Router()
    const user = require('../model/user')
    const jwt = require('jsonwebtoken')

    app.get(`/userCenter/:id`, (req, res) => {
        // user.updateOne({user:"zzp66"},{sex:"男"}).then(data=>{
        //     console.log(data)
        // }).catch(err=>{
        //     console.log(err)
        // })

        const token = String(req.headers.authorization || '').split(" ").pop()

        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                console.log(token)
                res.send({code: -2, msg: "token验证失败"})

            }
            user.findById(req.params.id).then((data) => {
                // console.log(req.params.id)
                // console.log(data)
                res.send({code:0,msg:'查询成功!',data})
            }).catch(err=>{
                console.log(err)
                res.send({code:1,msg:'服务器出错'})
            })
        })

    }, router)

    app.post("/userCenter/modify",(req,res)=> {
        const token = String(req.headers.authorization || '').split(" ").pop()
        console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
                console.log(data)
            }
            let reqData=req.body

            //先处理需要更新的数据
            let newData={
                user:reqData.user,
                phone:reqData.phone,
                email: reqData.email,
                age:reqData.age,
                sex:reqData.sex,
                signature:reqData.signature,
            }
            console.log(req.body.user)
            console.log(req.body.id)
            user.updateOne({_id:req.body.id},newData).then((data)=>{
                console.log(data)
                res.send({code:0,msg:"修改成功"})
            }).catch((err)=>{
                console.log(err)
                res.send({code:1,msg:'修改失败'})
            })
        })

    }, router)
}
