const express = require('express')
const router = express.Router()
const multer = require('multer')

/**
 * @api {post} /upload/photo  上传图片
 * @apiName upload/photo
 * @apiGroup User
 *
 * @apiParam {File} files 图片文件(图片不能超过500K).
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */


    var storage = multer.diskStorage({
        //设置上传后文件路径，uploads文件夹会自动创建。
        destination: function (req, file, cb) {
            cb(null, './static/image')
        },
        //给上传文件重命名，获取添加后缀名
        filename: function (req, file, cb) {
            console.log("---", file)
            //给图片加上时间戳格式防止重名名
            //比如把 abc.jpg图片切割为数组[abc,jpg],然后用数组长度-1来获取后缀名
            //当用户输入xxx.xx.xx.jpg时出现多个点
            // let ext=file.originalname.split('.')[1]//截取文件名的类型
            let exts = file.originalname.split('.')
            let ext = exts[exts.length - 1];//取到最后一个
            let tmpname = new Date().getTime() + parseInt(Math.random() * 9999)//时间戳来不同的时间,后面再加上随机数字防止在同一时间上传图片

            cb(null, `${tmpname}.${ext}`);
        }
    });
    var upload = multer({
        storage: storage
    });
    router.post('/upload', upload.single('file'), (req, res) => {

        console.log(req.file)
        let {size, mimetype, path} = req.file
        let types = ['jpg', 'jpeg', 'png', 'gif'];//允许上传的数据类型

        let tmpType = mimetype.split('/')[1]
        if (size > 500000) {
            return res.send({err: '-1', msg: '尺寸过大'})
        } else if (types.indexOf(tmpType) === -1) {
            return res.send({err: -2, msg: '媒体类型错误'})
        } else {
            //上传图片会返回一个图片的地址
            let url = `/public/image/${req.file.filename}`
            res.send({err: 0, msg: '上传成功', img: url})
        }
    })



module.exports = router
