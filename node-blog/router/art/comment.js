module.exports=app=>{
    const express = require('express')
    const router = express.Router()
    const jwt = require('jsonwebtoken')
    const comment =require('../../model/comment')
    const article = require('../../model/article')

    app.post("/article/comment", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token,app.get("secret"),(err,data)=>{
            if(err){
                res.send({code: -2, msg: "token验证失败"})
            }

            let reqData=req.body

            comment.create({
                author:reqData.authorId,
                content:reqData.content
            }).then(data=>{
                console.log(data)
                article.updateOne({_id:reqData.articleId},{$push:{comment:data._id}}).then(()=>{
                    res.send({code:0,msg:'评论成功!'});
                }).catch(err=>{
                    res.send({code:1,msg:"出现异常,请稍后再试!"})
                })
            }).catch(err=>{
                res.send({code:2,msg:"创建失败!"})
            })


        })
    },router)

    app.post("/article/recomment",(req,res)=>{
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token,app.get("secret"),(err,data)=> {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }
            let reqData=req.body
            comment
                .create({
                    author : reqData.authorId
                    ,content : reqData.content
                })
                .then(data=>{
                    //给一级评论添加关联
                    console.log(data._id)
                    comment
                        .updateOne(
                            {_id:reqData.commentId},
                            {$push:{comment:data._id}}
                        )
                        .then(()=>{
                            res.send({code:0,msg:"评论成功",data});
                        })
                        .catch(e=>{
                            console.log(e);
                            res.send({code:1,msg:"服务器异常……"});
                        })
                })
                .catch(e=>{
                    console.log(e);
                    res.send({code:1,msg:"服务器异常……"});
                })

        })
    },router)
}
