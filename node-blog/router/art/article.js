module.exports = app => {
    const express = require('express')
    const router = express.Router()
    const jwt = require('jsonwebtoken')
    const article = require('../../model/article')
    const cheerio = require("cheerio")
    //发表文章
    app.post("/article", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
                console.log(data)
            }
            let reqData = req.body
            console.log(reqData)
            article.create({
                title: reqData.title,
                tag: reqData.tag,
                content: reqData.content,
                author: reqData.id
            }).then((data) => {
                res.send({code: 0, mgs: '发表成功', data})
            }).catch(err => {
                console.log(err)
                res.send({code: 1, msg: "服务器异常,请稍后再试"})
            })

        })
    }, router)

    //查找所有文章
    app.get("/article", (req, res) => {
        article.find().populate("author").then((data) => {

            res.send({code: 0, msg: '查询成功', data})
        }).catch(err => {
            res.send({code: 1, msg: '查询失败'})
        })
    }, router)

    //单独每个文章的文章页
    app.get("/article/:id", (req, res) => {
        // console.log(req.params.id)
        article.findById(req.params.id).populate({path:'author',select:["user","_id","avatar"]}).populate({
            path:"comment",
            select : ["_id","comment","author","content","date","count1","like_users"],
            populate:{
                path:"author comment",
                populate : {
                    path : "author"
                }
            }
        }).then(data => {
            article.updateOne({_id: req.params.id},{$inc:{pageviews:1}},()=>{})

            res.send({code: 0, data})

        }).catch(err => {
            res.send({mgs: '出现异常问题!'})
        })
    },router)


    //文章搜索
    app.post("/article/getInfoByKw",(req,res)=>{
        let {kw}=req.body
        let reg=new RegExp(kw)

        article.find({$or:[{title:{$regex:reg}},{tag:{$regex: reg}}]}).populate("author").then((data)=>{
            res.send({code:0,msg:'查询成功!',data})

        }).catch(()=>{
            res.send({code:1,msg:'查询失败!'})
        })
    },router)


//    个人文章
    app.get("/userArticle/:id",(req,res)=>{
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
                // console.log(data)
            }
            let reqData=req.body
            article.find({author: req.params.id}).populate("author").then((data)=>{
                res.send({code:0,msg:'查询成功!',data})
            }).catch(()=>{
                res.send({code:1,msg:"查询失败!"})
            })
        })
    },router)

//    个人文章删除
    app.get("/del/:id",(req,res)=>{
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
                // console.log(data)
            }
            article.findOneAndDelete({_id:req.params.id}).then(()=>{
                res.send({code:0,msg:'删除成功'})
            }).catch(()=>{
                res.send({code:1,msg:'删除失败'})
            })
        })
    },router)
}
