module.exports = app => {
    const express = require('express')
    const router = express.Router()
    const jwt = require('jsonwebtoken')
    const article = require('../../model/article')
    const user = require('../../model/user')
    const comment = require("../../model/comment")

    //文章点赞和踩
    app.post("/like", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {articleId, authorId} = req.body
            // console.log(articleId,authorId,good)

            article.findOne({_id: articleId}).then(data => {
                // console.log(data)
                let fields = {};
                data.count1 = data.count1 + 1
                fields.count1 = data.count1

                let like_users_arr = data.like_users.length ? data.like_users : []

                user.findOne({_id: authorId}).then(data => {
                    // console.log(data)
                    let new_like_user = {}
                    new_like_user.id = data._id
                    new_like_user.good = true

                    like_users_arr.push(new_like_user)
                    fields.like_users = like_users_arr

                    article.updateOne({_id: articleId}, fields).then(data => {
                        // console.log(data)
                        res.send({code: 0, msg: '点赞成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '点赞失败'})
                    })
                })
            }).catch(error => {
                // console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)

    app.post("/cancel", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {articleId, authorId} = req.body
            // console.log(articleId,authorId,good)

            article.findOne({_id: articleId}).then(data => {
                // console.log(data)
                let fields = {};
                data.count1 = data.count1 - 1
                fields.count1 = data.count1

                let like_users_arr = data.like_users.length ? data.like_users : []

                user.findOne({_id: authorId}).then(data => {
                    // console.log(data)
                    let new_like_user = {}
                    new_like_user.id = data._id
                    new_like_user.good = false

                    like_users_arr.push(new_like_user)
                    fields.like_users = like_users_arr

                    article.updateOne({_id: articleId}, fields).then(data => {
                        // console.log(data)
                        res.send({code: 0, msg: '取消成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '取消失败'})
                    })
                })
            }).catch(error => {
                console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)

    app.post("/unlike", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {articleId, authorId} = req.body
            // console.log(articleId,authorId,good)

            article.findOne({_id: articleId}).then(data => {
                // console.log(data)
                let fields = {};
                data.count2 = data.count2 + 1
                fields.count2 = data.count2

                let unlike_users_arr = data.unlike_users.length ? data.unlike_users : []

                user.findOne({_id: authorId}).then(data => {
                    // console.log(data)
                    let unnew_like_user = {}
                    unnew_like_user.id = data._id
                    unnew_like_user.cancelGood = true

                    unlike_users_arr.push(unnew_like_user)
                    fields.unlike_users = unlike_users_arr

                    article.updateOne({_id: articleId}, fields).then(data => {
                        // console.log(data)
                        res.send({code: 0, msg: '踩成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '踩失败'})
                    })
                })
            }).catch(error => {
                console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)
    app.post("/uncancel", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {articleId, authorId} = req.body
            // console.log(articleId,authorId,good)

            article.findOne({_id: articleId}).then(data => {
                // console.log(data)
                let fields = {};
                data.count2 = data.count2 - 1
                fields.count2 = data.count2

                let unlike_users_arr = data.unlike_users.length ? data.unlike_users : []

                user.findOne({_id: authorId}).then(data => {
                    // console.log(data)
                    let unnew_like_user = {}
                    unnew_like_user.id = data._id
                    unnew_like_user.cancelGood = false

                    unlike_users_arr.push(unnew_like_user)
                    fields.unlike_users = unlike_users_arr

                    article.updateOne({_id: articleId}, fields).then(data => {
                        // console.log(data)
                        res.send({code: 0, msg: '踩成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '踩失败'})
                    })
                })
            }).catch(error => {
                // console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)

//    评论点赞
    app.post("/comment/like", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {commentId, authorId} = req.body
            console.log(commentId, authorId)

            comment.findOne({_id: commentId}).then(data => {
                console.log(data)
                let fields = {};
                data.count1 = data.count1 + 1
                fields.count1 = data.count1

                let like_users_arr = data.like_users.length ? data.like_users : []

                user.findOne({_id: authorId}).then(data => {
                    console.log(data)
                    let new_like_user = {}
                    new_like_user.id = data._id
                    new_like_user.good = true

                    like_users_arr.push(new_like_user)
                    fields.like_users = like_users_arr

                    comment.updateOne({_id: commentId}, fields).then(data => {
                        console.log(data)
                        res.send({code: 0, msg: '点赞成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '点赞失败'})
                    })
                })
            }).catch(error => {
                console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)

    //评论取消点赞
    app.post("/comment/cancel", (req, res) => {
        const token = String(req.headers.authorization || '').split(" ").pop()
        // console.log(token)
        jwt.verify(token, app.get("secret"), (err, data) => {
            if (err) {
                res.send({code: -2, msg: "token验证失败"})
            }

            let {commentId, authorId} = req.body
            console.log(commentId, authorId)

            comment.findOne({_id: commentId}).then(data => {
                console.log(data)
                let fields = {};
                data.count1 = data.count1 - 1
                fields.count1 = data.count1

                let like_users_arr = data.like_users.length ? data.like_users : []

                user.findOne({_id: authorId}).then(data => {
                    console.log(data)
                    let new_like_user = {}
                    new_like_user.id = data._id
                    new_like_user.good = false

                    like_users_arr.push(new_like_user)
                    fields.like_users = like_users_arr

                    comment.updateOne({_id: commentId}, fields).then(data => {
                        console.log(data)
                        res.send({code: 0, msg: '点赞成功!'})
                    }).catch(err => {
                        res.send({code: 1, msg: '点赞失败'})
                    })
                })
            }).catch(error => {
                console.log(error)
                res.send({code: 22, msg: '失败'})
            })


        })
    }, router)
}
