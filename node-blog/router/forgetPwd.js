module.exports=app=>{
    const express=require('express')
    const router=express.Router()
    const crypto = require("crypto")
    const user=require('../model/user')

    app.post("/forgetPWd",(req,res)=>{
        let reqData = req.body;

        let resetPwd = crypto.createHash("sha256").update(reqData.psd).digest("hex")
        let resetPwd2 = crypto.createHash("sha256").update(reqData.newPsd).digest("hex")
        user.findOne({user:reqData.user}).then((data)=>{
            console.log(data)
            if(data.email===reqData.email){
                if(reqData.code!=global.codes){
                    return res.send({code:3,msg : '验证码错误'})
                }else if(resetPwd !=resetPwd2){
                    return res.send({code:2,msg : '两次密码不相同'})
                }else{
                    user.updateOne({user: reqData.user}, {pwd: resetPwd}).then((data) => {
                        console.log(global.codes)
                        res.send({code: 0, msg: "修改成功"})

                    }).catch(() => {
                        res.send({code: 1, msg: "用户不存在"})
                    })
                }
            }
            else{
                res.send({code:5,msg:'邮箱与账号不匹配'})
            }
        }).catch(err=>{
            res.send({code:6,msg:'没有此用户'})
        })


    },router)
}

//点赞  +1  点赞的人在数据库加上  ,如果同一篇文章
