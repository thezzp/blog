module.exports=app=>{
    const express=require('express')
    const router= express.Router()
    const Mail=require('../utils/mail')
    const user=require('../model/user')

    global.codes={};

    app.post("/getMailCode",(req,res)=>{
        console.log(req.body)
        let {email}=req.body
        let code=parseInt( ('000000' + Math.floor(Math.random() * 999999)).slice(-6))// 产生随机码

        user.findOne({email:email}).then(data=>{

            if (data===null){
                res.send({code:2,msg:'邮箱不存在'})
            }
            Mail.send(email,code).then(()=>{
                console.log("1")
                codes=code
                console.log(codes)
                //将邮箱和邮箱匹配的验证码保存到缓存中】
                res.send({err:0,msg:'验证码发送ok'})
            }).catch(err=>{
                console.log(err)
                res.send({err:-1,msg:'验证码发送no ok'})
            })
        }).catch(err=>{
            res.send({code:1,msg:'出现异常,请稍后再试'})
        })
    },router)

}
