module.exports=app=>{
    const user=require('../model/user')
    const express=require('express')
    const router=express.Router()
    app.post('/register',(req,res)=>{
        let reqData=req.body;
        //检测该用户是否存在于数据库中
        user.findOne({user:reqData.user}).then(data=>{
            if(data){
                res.send({code:3,msg:"用户名已经存在"})
            }else{//不存在就注册
                if(reqData.pwd===reqData.pwd2){//密码是否一致
                    user.create({
                        user:reqData.user,
                        pwd:reqData.pwd,
                        email:reqData.email
                    }).then((data)=>{//创建成功
                        res.send({code:0,msg:"注册成功"})
                    }).catch((err)=>{
                        console.log(err)

                        res.send({code:4,msg:"注册失败",err})
                    })
                }else{
                    res.send({code:2,msg:'两次密码不相同'})
                }


            }
        }).catch(err=>{
            res.send({code:3,msg:"服务器异常,请稍后再试"})
        })
    },router)

}
