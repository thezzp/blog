module.exports = app => {
    const express = require('express')
    const router = express.Router()
    const crypto = require("crypto")
    const user = require('../model/user')
    const jwt = require('jsonwebtoken')
    app.post("/resetPwd", (req, res, next) => {
        let reqData = req.body
        const token = String(req.headers.authorization || '').split(" ").pop()

        jwt.verify(token, app.get('secret'), (err, data) => {
            if (err) {
                console.log(err)
                res.send({code: -2, msg: "token验证失败"})
            }
            console.log(data)

            user.find({_id: data.id}).then(data => {
                if (data) {
                    // console.log(data)
                    //用户存在
                    //检测原密码是否正确
                    let pwd = crypto.createHash("sha256").update(reqData.pwd).digest("hex")
                    console.log(pwd)
                    if (data[0].pwd === pwd) {

                        let newPwd = crypto.createHash("sha256").update(reqData.newPwd).digest("hex")
                        user.updateOne({_id: data[0].id}, {pwd: newPwd}).then(data => {
                            res.send({code: 0, msg: "修改成功"})
                        }).catch(() => {
                            res.send({code: 1, msg: "修改失败"})
                        })
                    } else {
                        res.send({code: 7, msg: "原密码错误,请重新输入"})
                    }
                } else {
                    //理论上,data是存在的,但是为了避免错误,还是要判断一下

                    req.send({code: 4, msg: "用户不存在,请重新登录"})
                }
            })
        })


    }, router)
}
