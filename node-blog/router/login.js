module.exports=app=>{
    const express=require('express')
    const router=express.Router()
    const crypto=require("crypto")
    const jwt=require('jsonwebtoken')
    const user=require('../model/user')
    //
    // user.create({user:'zzp66',age:22,pwd:'123456abcd',email:'1961465453@qq.com',phone:'13049081646',avatar: '/public/image/1586762711357.jpg'});


    app.post('/login',(req,res)=>{
        let reqData=req.body;
        user.findOne({user:reqData.user}).then(data=>{
            if(data){//data为真即注册过,继续密码比对
                let pwd=crypto.createHash("sha256").update(reqData.pwd).digest("hex")
                //判断加密后的密码和数据库的是否一致
                if(data.pwd===pwd){
                   let token=jwt.sign({id:data._id},app.get("secret"),{expiresIn: "7d"})
                    res.send({code:0,msg:"登录成功",token:token,data})

                    res.send({code:0,msg:"登录成功"})
                }else{
                    res.send({code:1,msg:'密码出错'})
                }
            }else{//代表user没有注册过,返回前端错误信息
                res.send({code:3,msg:"用户未注册"})
            }
        }).catch((err)=>{
            console.log(err)
            res.send({code:2,msg:"服务器异常,请稍后再试"})
        })
    },router)
}
