
import Vue from 'vue'

import VueEditor from 'vue2-editor'
export default ()=>{
  Vue.use(VueEditor)
}
