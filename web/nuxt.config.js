module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: "blog",
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''},
      {name:"referrer",content:"no-referrer"}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/blog.ico'},
      {rel: 'stylesheet', type: 'text/css', href: '//at.alicdn.com/t/font_1717789_hg5ffjqk1d.css'}
    ],
    script:[
      {src:"/live2dw/lib/L2Dwidget.min.js"},
      {src:"/live2dw/lib/L2Dwidget.0.min.js"},
      {src:"/aixin.js"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui',
    "@/plugins/axios",
    {src: "~plugins/vue-particles", ssr: false},
    { src: '@/plugins/localStorage', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: 'http://localhost:3002'
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [/^element-ui/],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
