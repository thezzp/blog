export const state = () => ({
  userInfo: {
    token: '',
    code: 0,
    data: {
      _id:'',
      user: "",
      avatar: '',
      sex:'',
      email:'',
      phone:'',
      age:'',
      sign:""
    }
  },
  code:0
});

export const mutations = {
//  设置用户的数据
  setUserInfo(state, data) {
    state.userInfo = data
  },
  //清除用户的数据
  clearUserInfo(state) {
    //  重置用户信息
    state.userInfo = {
      token: '',
      data: {
        user: "",
      }
    }
    localStorage.clear();
  },

  uploadInfo(state,data){
    state.userInfo.data.avatar=data.avatar
    state.userInfo.code=data.code
  }

//
};


//actions模块是用来存放公共异步的方法
export const actions = {

  login({commit}, data) {
    //返回出去loginForm中调用login 返回 promise(重点)
    return this.$axios({
      url: '/login',
      method: "post",
      data
    }).then(res => {

      //把解析出来的数据通过setUserInfo存到state中
      commit('setUserInfo', res.data)

      // 调用外部的成功的回调函数  resolve()是调用成功的函数
      Promise.resolve();

    })
  },



  upload({commit}, data) {
    return this.$axios({
      url: '/avatar',
      method: "post",
      data
    }).then(res=>{
      console.log(res.data)
      commit("uploadInfo",res.data)

      Promise.resolve()
    })
  }
}
