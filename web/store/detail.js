export const state=()=>({
  detailData: {}
})


export const mutations={
  setDetail(state,data){
    state.detailData=data
  }
}

export const actions={
  detail({commit},id) {
    return this.$axios({
      url:`/article/${id}`,
      method:'get'
    }).then(res=>{
      console.log(res.data.data)
      localStorage.setItem("comment",JSON.stringify(res.data.data.comment))
      commit('setDetail',res.data.data)
      Promise.resolve()
    })
  }
}
